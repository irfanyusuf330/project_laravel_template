<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeran1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peran1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('film_id');
            $table->foreign('film_id')->references('id')->on('film1');
            $table->unsignedBigInteger('cast_id');
            $table->foreign('cast_id')->references('id')->on('cast1');
            $table->string('nama',45);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peran1');
        // Schema::table('cast', function (Blueprint $table) {
        //     $table->dropColomn(['nama']);
        //     $table->dropForeign(['film_id']);
        //     $table->dropForeign(['user_id']);
        //     $table->dropColomn(['user_id']);
        //     $table->dropColomn(['id']);
        
           
                       
        // });
    }
}
