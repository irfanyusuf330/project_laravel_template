@extends('master')

@section('content')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New Cast</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/cast" method="POST">
              @csrf 
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">nama</label>
                    <input type="text" class="form-control" id="title" name="nama" placeholder="Enter nama">
                  </div>
                  <div class="form-group">
                    <label for="umur">umur </label>
                    <input type="text" class="form-control" id="umur" name="umur" placeholder="umur">
                  </div>
                  <div class="form-group">
                    <label for="bio">bio </label>
                    <input type="text" class="form-control" id="bio" name="bio" placeholder="bio">
                  </div>                   
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Daftar</button>
                </div>
              </form>
            </div>

@endsection