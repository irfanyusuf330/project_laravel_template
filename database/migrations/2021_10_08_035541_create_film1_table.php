<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilm1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul', 45);
            $table->text('text');
            $table->integer('tahun');
            $table->string('poster', 45);
            $table->unsignedBigInteger('genre_id');
            $table->foreign('genre_id')->references('id')->on('genre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film1');
        // Schema::table('film', function (Blueprint $table) {
        //     $table->dropTimestamps();
        //     $table->dropForeign(['genre_id']);
        //     $table->dropColomn(['poster']);         
        //     $table->dropColomn(['tahun']);
        //     $table->dropColomn(['text']);         
        //     $table->dropColomn(['judul']);
        //     $table->dropColomn(['id']);          
        // });
    }
}
